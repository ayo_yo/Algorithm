# coding=utf-8
import functools

import numpy as np


@functools.lru_cache()
def fib(n):
    if n <= 2:
        return 1
    return np.mod(np.add(fib(n - 1), fib(n - 2)), 10)


c = 0
for i in range(1, 1000):
    c += 1
    # fib()本身自带%10，仅取个位
    print(fib(i), end=' ' if c % 60 != 0 else '\n')

print(202202011200 / 60 * 8)
